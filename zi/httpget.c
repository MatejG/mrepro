 


#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>

#include "../wrapperi_i_headeri/mreFunkcije.h"

#define MAXLEN 10

#define BACKLOG 10







struct URLpair{
	
	struct sockaddr adress;
	char hostName[40];
	char filePath[40];
	
	
	};

struct URLpair parovi[3];
struct pollfd *pfdsG ;
int fd_count = 0;


int fileWork(int index, int mode){                  //3 modes 0--init get initial files,1--refresh append new bytes if present,2--restart get all files again, 3--get size of file
													//ret values: -1 file not present on remote server,  returns number of new bytes received
													
	 
		FILE * filefd;
		char buffer[RECBUFFER_SIZE];  //4KB
		char* header;
		int fileSize=0;                 //default file size
		int totalBytesReceived=0;
		int bytesReceived;
		char fileName[10];
		char *status;
		int sockTCP;
		int headersize;
		snprintf(fileName,10,"log%d",index+1);
		
		switch(mode){
			
			case 0:
			case 2:
				filefd=fopen(fileName,"wb");
				break;
				
			case 1:
			case 3:
				filefd=fopen(fileName,"ab");
				fileSize=ftell(filefd);
				printf("File size %d",fileSize);
				break;
				
			default:
				return -2;
			
		}//end switch
		
			
		if(filefd==NULL)
			errx(-2, "Local file open error\n");
		
		if( mode==3){
			fclose(filefd);
			return fileSize;
			}
			
			
			
		sockTCP= Socket(AF_INET, SOCK_STREAM, 0, 2);
			
		Connect(sockTCP, &parovi[index].adress, sizeof(parovi[index].adress), 3);
		header=(char *) malloc(100* sizeof(char));
		
		snprintf(header,100,"GET /%s HTTP/1.1\r\n"
						"Host: %s\r\n"
						"Range: bytes=%d-\r\n"
						"Connection: close\r\n\r\n",
						parovi[index].filePath,
						parovi[index].hostName,
						fileSize);
		
		Send(sockTCP,header,strlen(header),0,5);             //send header information
	
		bytesReceived=Recv(sockTCP,buffer,RECBUFFER_SIZE,0,6);    //receive chunk with header
		printf("Bytes with header = %d", bytesReceived);
		if(bytesReceived==0){
			fclose(filefd);
			errx(7,"Remote server error\n");
			}
		header=strtok(buffer,"\r\n\r\n");              //separate header from data                
		strcpy(buffer,strtok(NULL,""));                 //copy data to buffer                <== ne radi jer je rad sa stringovima jako praktican u C-u......
		strtok(header," ");
		headersize=strlen(header);
		status=strtok(NULL," ");               //server status
		if(strcmp(status,"404")==0){
			fclose(filefd);
			if(mode !=0){
				printf("Server doesnt have resource\n");
				return -1;
				}
			errx(8,"Remote server doesnt have resource\n");
		}else if(strcmp(status,"416")==0) {
			fclose(filefd);
			return 0;
			}
			
		int check= fwrite(buffer,1,bytesReceived-headersize,filefd);    //write first chunk
		printf("Bytes written in first chunk %d\n",check);
		totalBytesReceived+=bytesReceived-headersize;
		while((bytesReceived = Recv(sockTCP,buffer,RECBUFFER_SIZE,0,15)) > 0 ){    
			fwrite(buffer, 1, bytesReceived, filefd);
			printf("Bytes received : %d\n", bytesReceived);
			totalBytesReceived+=bytesReceived;
			memset(&buffer,0,bytesReceived);
			}
		fclose(filefd);
		close(sockTCP);
		printf("File succesfully trasfered\n");
		return totalBytesReceived;
	
	
	
	
	}


void closeSocketsAndFreeMemory (struct pollfd *pfds[], int fd_count){
	
	for(int i=0 ; i<fd_count; i++)
		close((*pfds)[i].fd );
	free(*pfds);
	
	}

void parseURL(char * url, int index){
	struct addrinfo hints, *res;
	char adress[40];
	char IP[INET_ADDRSTRLEN];
	char file[40];
	memset(&hints,0,sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC; 
	hints.ai_socktype = SOCK_STREAM; 
	
	strtok(url,"//");    //http part    
	strcpy(adress,strtok(NULL,"/"));
	strcpy(file,strtok(NULL,""));
	
	Getaddrinfo(adress,"80", &hints, &res,1);
	strcpy(parovi[index].hostName,adress);
	parovi[index].adress=*(res->ai_addr);
	strcpy(parovi[index].filePath,file);
	struct sockaddr *proba= &parovi[index].adress;
	Inet_ntop(AF_INET, &((struct sockaddr_in *) proba) -> sin_addr, IP, 
	INET_ADDRSTRLEN,3 );

	printf("Adresa %s IP %s Path %s\n", adress,IP,file);
	
	}



int main (int argc, char *argv[])
{
	//variable declaration
	
	char buf[MAXLEN];
	struct sockaddr cli;
	socklen_t clilen;
	int sfdUDP,ch ,msglen, listener, newfd;
	struct sockaddr their_addr;
    socklen_t sin_size;
	int yes=1;
	int poll_count;	
	int fd_size = 5;
	int UDPports[3];
	struct sockaddr_in setup;
	uint16_t TCP=80;
	int filesize=0;
	char * response;
	
	//variable initialisation

		
	
	
	if(argc != 7)
		errx(-1, "Usage: httpget url1 p1 url2 p2 url3 p3\n");
	int j=0;
	int k=0;
	for(int i = 1;i<argc;i++){
		
		if(i%2==0){                 //port on even arg number
			UDPports[j]=atoi(argv[i]);
			printf("PORT %d\n", UDPports[j]);
			j++;
			}
		else{
			parseURL(argv[i],k);
			k++;
			}
		
		
		}
	
	
	
	pfdsG=malloc(sizeof *pfdsG * fd_size);
	response= (char*)malloc(50*sizeof(char));
	

	//setup UDP server ports and polling variables
	
	for(int i=0;i<3;i++){
		
		setup.sin_family = AF_INET;
		setup.sin_port = htons (UDPports[i]);
		setup.sin_addr.s_addr = htonl (INADDR_ANY);
		sfdUDP = Socket(PF_INET , SOCK_DGRAM, 0, 1);
		Bind(sfdUDP,(struct sockaddr *) &setup,sizeof(setup),2);
		pfdsG[i].fd = sfdUDP;
		pfdsG[i].events = POLLIN;
		
		}
		fd_count= 3;  
	

	
	//get files 
	
	for(int i=0;i<3;i++)	
		fileWork(i,0);
		
	

	while (1){
		printf("Waiting for input\n");
		//indefinately wait for sockets or user input;
		poll_count = Poll(pfdsG, fd_count, -1, 5);
		
		for(int i=0; i<fd_count; i++){
			
			if(poll_count==0)   //all triggers have been solved
				break;
			
			if (pfdsG[i].revents & POLLIN) {
				
				clilen = sizeof(their_addr);
				msglen = recvfrom(pfdsG[i].fd, buf, MAXLEN, 0, &their_addr, &clilen);
				poll_count--;
				if(strncmp(buf,"stat\n",5) == 0){
					printf("Received stat request\n");
					filesize=fileWork(i,3);
					snprintf(response,50,"+%d\n",filesize);
					sendto(pfdsG[i].fd, response , strlen(response) ,0, &their_addr, 
					clilen);	
					
				}else if(strncmp(buf,"refresh\n",8) == 0){
					printf("Received refresh request\n");
					filesize=fileWork(i,1);
					if(filesize==-1)
						sendto(pfdsG[i].fd, "-\n" , 2,0, &their_addr, 
						clilen);	
					else{
					
						snprintf(response,50,"+%d\n",filesize);
						sendto(pfdsG[i].fd, response , strlen(response) ,0, &their_addr, 
						clilen);
					
						}	
					
				}else if(strncmp(buf,"restart\n",8) == 0){
					printf("Received restart request\n");
					filesize=fileWork(i,2);
					if(filesize==-1)
						sendto(pfdsG[i].fd, "-\n" , 2,0, &their_addr, 
						clilen);	
					else{
					
						snprintf(response,50,"+%d\n",filesize);
						sendto(pfdsG[i].fd, response , strlen(response) ,0, &their_addr, 
						clilen);
					
						}
						
				}else{
					sendto(pfdsG[i].fd, "-\n" , 2,0, &their_addr, 
					clilen);	
					
					}	
					
				
					
				} //end if for finding trigered fd
		
			}//end of polling for loop
		
	}//end of while


}//end of main
