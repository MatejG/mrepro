


#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in, INADDR_ANY
#include <unistd.h> // sleep
#include <string.h> // memset
#include <netdb.h> // addrinfo
#include <arpa/inet.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <err.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include "../wrapperi_i_headeri/mreFunkcije.h"


#define MAXPORT 22
#define MAX_PAYLOAD 512
#define ZAPIS_SIZE 38
#define ROUTER_PORT "520"
#define IP_SIZE 4



struct zapis {
	u_int16_t adress_family;
	u_int16_t route_tag;
	int IP ;
	int subNet;
	int nextHop;
    int metric;
};
    
struct RIP{
	u_int8_t command;
	u_int8_t version;
	u_int16_t zero;
	struct zapis entry[25];
};


int main(int argc, char **argv){
	
	//variable initialisation
	char * strc;
	char * Argadresa;
	int sockId, primljeno, buffersize, numZapis;
	struct addrinfo hints,*res;
	struct sockaddr * adresa_za_slanje;
	struct RIP poruka;   
	int progReceived = 0;
	int ON;
	int broj_zapisa;
	int vel_zapis = sizeof(struct zapis);
	char IP_presentation[INET_ADDRSTRLEN];
	char Subnet_presentation[INET_ADDRSTRLEN];
	int metrika;
	

	

	// argument checks 
	if(argc==1){  
		errx(-1,"Usage: ripreq IP_address\n");
	}
		//variable initialisation
	Argadresa = argv[1];
	printf("Adress = %s \n", Argadresa);
	buffersize= sizeof(struct RIP);
	memset(&hints, 0, sizeof(struct addrinfo ));
	memset(&poruka, 0, buffersize);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	
	//setup za poruku prema routeru
	poruka.command = 1;
	poruka.version = 2;
	poruka.zero = 0;
	
	poruka.entry[0].adress_family = htons(AF_UNSPEC);
	poruka.entry[0].route_tag = 0;
	poruka.entry[0].metric= htonl(16);
	

	
	Getaddrinfo(Argadresa, ROUTER_PORT, &hints, &res,1);
	sockId = Socket( res -> ai_family , res->ai_socktype, res->ai_protocol,2);
	
	adresa_za_slanje = res -> ai_addr;
	
	
	sendto(sockId, &poruka , 
		buffersize -24 * vel_zapis , 
		0, 
		adresa_za_slanje ,  
		sizeof(*adresa_za_slanje));  
	
	freeaddrinfo(res);
	printf("Cekam odgovor\n");
	
	primljeno = recvfrom(sockId, &poruka, buffersize, 0, NULL, NULL);
	broj_zapisa = (primljeno-4)/vel_zapis;
	printf("Broj primljenih zapisa %d \n", broj_zapisa);
	
	
	
	for(int i=0; i<broj_zapisa; i++){
		
		Inet_ntop(AF_INET, &(poruka.entry[i].IP), IP_presentation, 
		INET_ADDRSTRLEN,3 );
		Inet_ntop(AF_INET, &(poruka.entry[i].subNet), Subnet_presentation, 
		INET_ADDRSTRLEN,3);
		metrika= ntohl(poruka.entry[i].metric);
		printf("%s/%s metrika:  %d\n",IP_presentation,Subnet_presentation,metrika);
		}
		
	printf("Shuting down\n");
	close(sockId);
	return 0;
		
}//end main 






