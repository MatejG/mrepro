#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#define MIN_ARG 2



	int UDP = 0;
	int hex = 0;
	int networkOrder = 0;
	int IPv6 = 0;
	
	
	void getNameFromIp(char * adress, char * service ){
		char host[NI_MAXHOST];
		int error;
		struct sockaddr_in6 sa6;
		struct sockaddr_in sa4;
		char serviceName[NI_MAXSERV];
		int portNum;
		int flags=0;
		
		flags |= NI_NAMEREQD;
		if(UDP) 
			flags |= NI_DGRAM; 
		
		if((portNum = atoi(service)) == 0 )
			errx(1, "Invalid port number");
			
		
		if(IPv6){
			
			sa6.sin6_family = AF_INET6;
			sa6.sin6_port = htons( portNum);
			
			
			if(inet_pton(AF_INET6, adress, &(sa6.sin6_addr)) != 1)
				errx(3, "%s is not a valid IP address", adress);
			
			error = getnameinfo( (struct sockaddr *) &sa6, sizeof(struct sockaddr_in6)
			,host, NI_MAXHOST, serviceName, NI_MAXSERV, flags);
			if(error)
				errx(2, "nodename nor servname provided, or not known");
				
		} else{
			
			sa4.sin_family = AF_INET;
			sa4.sin_port = htons( portNum);
			
			
			if(inet_pton(AF_INET, adress, &(sa4.sin_addr)) != 1)
				errx(3, "%s is not a valid IP address", adress);
			
			error = getnameinfo( (struct sockaddr *) &sa4,sizeof(struct sockaddr_in), host, NI_MAXHOST,
			serviceName, NI_MAXSERV, flags);
			if(error)
				errx(2, "nodename nor servname provided, or not known");
			
			
			}
				
			printf("%s (%s) %s\n", adress, host, serviceName);
		
	}
	
	void getIpFromAdress(char * adress, char * service){

		struct addrinfo hints, *res, *pamti;
		int error;
		char IP[100];
		int port;
	
		memset(&hints,0,sizeof(struct addrinfo));
	
		if(IPv6) 
			hints.ai_family= AF_INET6;
		else
			hints.ai_family= AF_INET;
	
		if(UDP){
			hints.ai_protocol =IPPROTO_UDP;
			hints.ai_socktype =  SOCK_DGRAM;
		} else {
			hints.ai_protocol =IPPROTO_TCP;
			hints.ai_socktype = SOCK_STREAM;
		}
		hints.ai_flags |= AI_CANONNAME;
		error = getaddrinfo(adress,service,&hints, &res);
		
		if(error)
			errx(2,"nodename nor servname provided, or not known");
		pamti =res;
		while(res){
			if(IPv6){
				inet_ntop( res-> ai_family, 
				&((struct sockaddr_in6 *) res-> ai_addr) -> sin6_addr, 
				IP,100);
				port = 
				(int) ((struct sockaddr_in6 *) res-> ai_addr) -> sin6_port;
			} else {
				inet_ntop( res-> ai_family, 
				&((struct sockaddr_in *) res-> ai_addr) -> sin_addr, 
				IP,100);
				port = 
				(int)((struct sockaddr_in *) res-> ai_addr) -> sin_port;
			}
			if(!networkOrder)
				port = ntohs(port);
			if(hex)
				printf ("%s (%s) %04x\n", IP,res->ai_canonname,port);
			else
				printf ("%s (%s) %d\n", IP,res->ai_canonname,port);
				
			res= res -> ai_next;
		}
		freeaddrinfo(pamti);	
	}
	

int main( int argc, char *argv[]){
	
	char * adress;
	char * service;
	int opt;
	int flagTransport = 0;
	int flagIP = 0;
	int flagOrder = 0;
	int reverse = 0;
	
	
	if(argc==1){   // no argument or option call (display info)
		printf("prog [-r] [-t|-u] [-x] [-h|-n] [-46] {hostname|IP_address} {servicename|port}\n");	
		return 1;
		}

	
	while((opt=getopt(argc, argv,"tuxhn46r"))!=-1){
		switch(opt){                              
			
			case 't': 
				if(flagTransport) errx(1, "Invalid TCP/UDP option");
				flagTransport=1;
				break;
			case 'u':
				if(flagTransport) errx(1, "Invalid TCP/UDP option");
				flagTransport=1;
				UDP=1;
				break;
			case 'x':
				hex=1;
				break;
			case 'h' :
				if(flagOrder) errx(1, "Invalid order option");
				flagOrder=1;
				break;
			case 'n' :
				if(flagOrder) errx(1, "Invalid order option");
				flagOrder=1;
				networkOrder = 1;
				break;
			case '4' :
				if(flagIP) errx(1, "Invalid IP protocol option");
				flagIP= 1;
				break;
			case '6':
					if(flagIP) errx(1, "Invalid IP protocol option");
					flagIP=1;
					IPv6=1;
					break;
			case 'r' :
				reverse=1;
				break;
			default:
				errx(1,"Unknown option");
			}	
			
	}//end while
	
				
	if((argc - optind)!= MIN_ARG) errx(2, "nodename nor servname provided, or not known");
	
	adress = argv[argc-2];
	service = argv[argc-1];
	
	if(reverse)
		getNameFromIp(adress, service);
	else
		getIpFromAdress(adress, service);
		
	return 0;		
}//end of main 
