


#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>

#include "../wrapperi_i_headeri/mreFunkcije.h"

#define MAXLEN 6
#define S_PORT "1234"
#define MAX_PAYLOAD 1023
#define DEF_PAYLOAD_SIZE 8
#define BACKLOG 10
#define SERVER_ADRESS "10.0.0.20"




char payload[1024];


void closeSocketsAndFreeMemory (struct pollfd *pfds[], int fd_count){
	
	for(int i=0 ; i<fd_count; i++)
		close((*pfds)[i].fd );
	free(*pfds);
	
	}


void userInput(struct pollfd *pfds[],int fd_count ){
	
	char msg[MAX_PAYLOAD+10];
	fgets(msg, MAX_PAYLOAD+10, stdin);
	printf("Input received: %s\n", msg);
	if (strncmp(msg, "PRINT",5) == 0) {
		printf("Current payload : %s\n\n", payload);	
		
	} else if (strncmp(msg, "SET", 3) == 0){
		strtok(msg," ");
		strcpy(payload,strtok(NULL," "));
		printf("New payload is : %s\n\n", payload);
		
	} else if (strncmp(msg, "QUIT",4) == 0){
		closeSocketsAndFreeMemory(pfds,fd_count);
		printf("Shutting down\n\n");
		exit(0);
	}else{
		printf("Unknown command, ignoring\n\n");
		}

}



int main (int argc, char *argv[])
{
	//variable declaration
	
	char buf[MAXLEN];
	char *UDPport = S_PORT;
	char *TCPport = S_PORT;
	struct sockaddr cli;
	struct addrinfo hints, *res;
	socklen_t clilen;
	int sfdUDP,ch ,msglen, listener, newfd;
	struct sockaddr_in their_addr;
    socklen_t sin_size;
	int yes=1;
	int poll_count;
	char userMSG[50];	
	int fd_count = 0;
	int fd_size = 5;
	struct pollfd *pfds ;
	strcpy(payload, " \n");
		
	//variable initialisation

		
	while((ch = getopt(argc, argv, "t:p:u:")) != -1){
		switch(ch){
			case 't': 
					TCPport = optarg;
					break;
					
			case 'u':
					UDPport=optarg;
					break;
				
			case 'p': 
					if(strlen(optarg)> MAX_PAYLOAD){
						errx(1,"payload too large");
					}
					strcpy(payload, optarg);
					payload[strlen(optarg)+1] = '\n';
					break;
			default: 
					fprintf( stderr,  "./server [-t tcp_port] [-u udp_port] [-p popis]\n");
					return -1;
		}
	}
	
	printf("Payload = %s\n", payload);
	printf("UDPport = %s\n", UDPport);
	printf("TCPport = %s\n", TCPport);
	
	pfds=malloc(sizeof *pfds * fd_size);
	
	

	//setup UDP server ports and bind 
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 
	Getaddrinfo(SERVER_ADRESS,UDPport, &hints, &res,1);
	sfdUDP = Socket(res -> ai_family , res->ai_socktype, res->ai_protocol, 2);
	Bind(sfdUDP,res->ai_addr,res->ai_addrlen,3);
	
	
	//setup TCP server ports and binds 
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; 
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; 
	Getaddrinfo(SERVER_ADRESS,TCPport, &hints, &res,1);
	listener = Socket(res -> ai_family , res->ai_socktype, res->ai_protocol, 2);
	setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
	Bind(listener,res->ai_addr,res->ai_addrlen,3);
	Listen(listener,BACKLOG, 4);
	
	freeaddrinfo(res);
	
	//setup polling variables
	pfds[0].fd = 0;        //stdin
	pfds[0].events = POLLIN; // Report ready to read on incoming connection; 
	pfds[1].fd=listener;      //TCP listener
	pfds[1].events = POLLIN;
	pfds[2].fd= sfdUDP;       //UDP listener
	pfds[2].events = POLLIN;
	fd_count= 3;  
	
	
	
	

	
	//working loop
	while (1){
		printf("Waiting for input\n");
		//indefinately wait for sockets or user input;
		poll_count = Poll(pfds, fd_count, -1, 5);
		
		for(int i=0; i<fd_count; i++){
			
			if(poll_count==0)   //all triggers have been solved
				break;
			
			if (pfds[i].revents & POLLIN) {
				
				
				switch(i){
					
					case 0 :
						printf("User data received\n");
						userInput(&pfds,fd_count);
						poll_count--;
						break;
						
					case 1 :
						printf("New TCP connection\n");
						sin_size = sizeof their_addr;
						newfd = Accept(listener, (struct sockaddr *)&their_addr
						,&sin_size,6);
						add_to_pfds(&pfds,newfd,&fd_count,&fd_size);
						poll_count--;
						break;
						
					case 2:
						printf("UDP bot request received\n");
						clilen = sizeof(cli);
						msglen = recvfrom(sfdUDP, buf, MAXLEN, 0, &cli, &clilen);
						poll_count--;
						if(strcmp(buf,"HELLO\n") == 0){
							printf("Received valid request from bot\n\n");
							
							sendto(sfdUDP, payload, strlen(payload), 0, &cli, clilen);
							}
						else 
							printf("Received invalid message from bot \n\n");
						break;
							
					default:
						printf("Receiving data from TCP connection\n");
						Recv(pfds[i].fd, buf, MAXLEN, 0, 7);
						poll_count--;
						printf("TCP msg received: %s\n", buf);
						if(strcmp(buf,"HELLO\n") == 0){
							printf("Received valid request from bot\n\n");
							Send(pfds[i].fd, payload, strlen(payload), 0, 8);
							close(pfds[i].fd);
							del_from_pfds(pfds, i, &fd_count);
							}
						else {
							printf("Received invalid message from bot \n\n");
							close(pfds[i].fd);
							del_from_pfds(pfds, i, &fd_count);
							}
							
							
					} //end switch case;
					
				} //end if for finding trigered fd
		
			}//end of polling for loop
		
	}//end of while loop
	
}//end of main
