

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>
#include <pthread.h>
#include <syslog.h>
#include <errno.h>
#include <ctype.h> 
 
#include "../wrapperi_i_headeri/mreFunkcije.h"



int threadsActive=0;
int demon=0;
pthread_mutex_t lock;

struct DATA{
	uint16_t code;
	uint16_t blocknum;
	char data[512];
	};
	
struct ACK {
	uint16_t code;
	uint16_t blocknum;
	};

struct RRQbuffer{
	u_int16_t code;
	char fileNameAndMode[100];
	};
	

	
struct threadArgs{
	
	struct sockaddr cli;
	int clilen;
	char filename[80];
	char mode[30];
	
	};
	


int sendProtocol(int sockfd, struct DATA toSend, struct sockaddr cli, 
	int clisize, uint16_t* currentBlock, int*index){                              //resets index to 0 when transfer succesful and increments currentBlock
	struct ACK ack;
	socklen_t clilen= (socklen_t) clisize;
	for(int j=0;j<4;j++){
		sendto(sockfd,&toSend , 2*sizeof(uint16_t)+*index, 0, &cli, clilen);
		if(Recvfrom(sockfd,&ack,sizeof(ack),0, &cli, &clilen) ==-1)   //didnt get conformation in time
			continue;					
		if((*currentBlock == ntohs(ack.blocknum)) && (ntohs(ack.code)==4)){  //received ack
			(*currentBlock)++;
			*index=0;
			return 0;
			}
			
		}
		return -1; // retransmission failed
						
	
	}

	

int sendAscii(FILE * filefd, int sockfd,struct sockaddr cli, int clilen){
	uint16_t currentBlock=1;
	int index=0;
	struct DATA toSend;
	int bytesRead;
	int lastChunk=0;    //flag
	char buffer[RECBUFFER_SIZE];
	toSend.code=htons(3);
	
	fseek(filefd, 0, SEEK_SET);  
	
	while ((bytesRead = fread(buffer, 1, RECBUFFER_SIZE, filefd)) > 0){     //process data from file to socket in 4KB chunks
		
		if(bytesRead<RECBUFFER_SIZE)  //last chunk in file 
			lastChunk=1;
		
		
		for(int i=0; i<bytesRead;i++){   //go byte by byte 
			
			if(buffer[i]=='\n' && buffer[i-1]!='\r'){    //found \n char
						
				toSend.data[index]='\r';
				index++;
				if(index==512){         //if data full  
					toSend.blocknum = htons(currentBlock);
					if(sendProtocol(sockfd,toSend,cli,clilen,&currentBlock,&index)==-1){    // error
						return -1;
						}			
					}
				toSend.data[index]='\n';
				index++;
				if(index==512 || ((i == bytesRead -1) && lastChunk)){         //if data full  or last byte 
					toSend.blocknum = htons(currentBlock);
					if(sendProtocol(sockfd,toSend,cli,clilen,&currentBlock,&index)==-1){    // error
						return -1;
						}
					if((i == bytesRead -1) && lastChunk)   //finished
						return 0;
					
					}
					
				continue;
				}//end \n if
				
			toSend.data[index]=buffer[i];
			index++;
			if(index==512 || ((i == bytesRead -1) && lastChunk)){         //if data full  or last byte 
				toSend.blocknum = htons(currentBlock);
				if(sendProtocol(sockfd,toSend,cli,clilen,&currentBlock,&index)==-1){    // error
					return -1;
					}
			
					
				}
					
				
				                                                                   
			}//end for
			
		}	//end while
		
		
		return 0;
	
	}//end sendAscii
	
	
int sendOctet(FILE * filefd, int sockfd,struct sockaddr cli, int clilen){
	
	uint16_t currentBlock=1;
	struct DATA toSend;
	int bytesRead;
	toSend.code=htons(3);
	

	while ((bytesRead = fread(toSend.data, 1, 512, filefd)) > 0){     //process data from file to socket in 4KB chunks
		
		toSend.blocknum = htons(currentBlock);
		if(sendProtocol(sockfd,toSend,cli,clilen,&currentBlock,&bytesRead)==-1){    // error
			return -1;
			}					
		}//end while
			                                                                
	return 0;

	}
	

void *sendData(void *arg) {

	int sockfd;
	FILE * filefd;
	struct threadArgs localcpy= *((struct threadArgs*)arg);
	struct timeval tv;

	tv.tv_sec = 1;  	
	
	sockfd= Socket (localcpy.cli.sa_family, SOCK_DGRAM,0,6);
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));
	
	if(checkFileTftp(demon,localcpy.mode,localcpy.filename, sockfd, localcpy.cli,localcpy.clilen, &filefd))  //if error exit thread
		pthread_exit(NULL);
	
	if(strcmp(localcpy.mode,"netascii")==0)
		sendAscii(filefd, sockfd,localcpy.cli,localcpy.clilen);
	else
		sendOctet(filefd,sockfd,localcpy.cli,localcpy.clilen);

	close(sockfd);
	fclose(filefd);
	pthread_mutex_lock(&lock);
	threadsActive--;
	pthread_mutex_unlock(&lock);
	pthread_exit(NULL);
	
		
		
		
		
		}


int main(int argc, char *argv[] ) {
    int sockfd;
    struct addrinfo hints, *res;
    struct sockaddr cli;
    int code;
    socklen_t clilen;
    int ch;
	char *port ;
	char mode[30];
	char *mod;
	char *fileName;
    int error;
    struct RRQbuffer fileNameBuffer;
    int one = 1;
    char IP_presentation[INET_ADDRSTRLEN];
    FILE * localfile;
    int bytesRead;
    struct ERROR err;
	struct threadArgs *args;
    fileName= malloc(80);
	pthread_t tid;
    	
    
    while((ch = getopt(argc, argv, "d")) != -1){
		switch(ch){
			case 'd': 
					demon=1;
					break;
		
			default: 
					return 4;
		}
	}
	
	if(argc==demon+1){   // no argument or option call (display info)
		printf("Usage: tftpserver [-d] port_name_or_number\n");	
		return 1;
		}
		
	port=argv[argc-1];
	chdir("/");
	if(demon)
		daemon_init ("mg511232:MrePro", LOG_FTP);
	
	
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype= SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;
	args=malloc(sizeof(struct threadArgs));
	
    Getaddrinfo(NULL, port, &hints, &res,5);
    sockfd = Socket(res -> ai_family, res -> ai_socktype, res -> ai_protocol, 6);
    
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	Bind(sockfd,res->ai_addr,res->ai_addrlen,7);
	freeaddrinfo(res);

    //accept while loop
    
    while(1){
		
		clilen = sizeof(cli);
			
		recvfrom(sockfd, &fileNameBuffer, sizeof(struct RRQbuffer), 0, &cli, &clilen);
		
		switch(code= ntohs(fileNameBuffer.code)){
			
			case 1:
				
				mod = fileNameBuffer.fileNameAndMode;
				Inet_ntop(AF_INET, &cli, IP_presentation, 
				INET_ADDRSTRLEN,3 );
				fileName= strtok(fileNameBuffer.fileNameAndMode,"\0");
				strcpy(mode,strlen(fileName)+1+mod);
				if(demon)
					syslog(LOG_INFO,"%s -> %s mode %s\n ", IP_presentation,fileName, mode);
				else
					printf("%s -> %s mode %s\n ", IP_presentation,fileName, mode);
				
				if(threadsActive<50){
					
					for(int i = 0; mode[i]; i++){
						mode[i] = tolower(mode[i]);
						}
					strcpy(args->mode,mode);
					strcpy(args->filename, fileName);
					args->cli=cli;
					args->clilen=clilen;
					
					pthread_mutex_lock(&lock);
					threadsActive++;
					pthread_mutex_unlock(&lock);	
					pthread_create(&tid,NULL,sendData,(void *)args);
				}else{
					
					
					if(demon)
						syslog(LOG_INFO,"TFTP ERROR 0 Serving too many clients");
					else
						fprintf(stderr,"TFTP ERROR 0 Serving too many clients\n");
					err.code=htons(5);
					err.errorCode=0;
					err.zero='0';
					strcpy(err.message, "Serving too many clients\0");
					sendto(sockfd,&err , 5+  strlen(err.message), 0, &cli, clilen);	
					}
				break;
				
				
				
				
			default :
			
				if(demon)
					syslog(LOG_INFO,"TFTP ERROR 0 Faulty code or not implemented");
				else
					fprintf(stderr,"TFTP ERROR 0 Faulty code or not implemented\n");
				err.code=htons(5);
				err.errorCode=0;
				strcpy(err.message, "Faulty code or not implemented\0");
				sendto(sockfd,&err , 5+ strlen(err.message), 0, &cli, clilen);

			}
			
	
	

		}//end accept while loop
	return 0;
}
