

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in, INADDR_ANY
#include <unistd.h> // sleep
#include <string.h> // memset
#include <netdb.h> // addrinfo
#include <arpa/inet.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <err.h>
#include <sys/types.h>
#include <stdlib.h>
#include <poll.h>
#include <signal.h>
#include <syslog.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

#include "../wrapperi_i_headeri/strukture.h"

int Recvfrom(int sfd,void *buff, size_t nbytes,int flags, 
struct sockaddr* cli, socklen_t *len){
	int n;
	n = recvfrom(sfd,buff,nbytes,flags,cli, len);
	if (n == -1) {
		if (errno == EAGAIN) { // "Resource temporarily unavailable"
			return -1;
		} 
	
	}
	return n;
}



int checkFileTftp(int daemon, char * mode, char * fileName, int sockfd, struct sockaddr cli, int clilen ,FILE ** file){
	   
	char temp[100];
	struct ERROR err;
	char *result;
	char *token;
	strcpy(temp, fileName);
	if( strchr(fileName, '/')!= NULL){
		token=strtok(temp, "/");
		if(strcmp(token,"tftpboot")!=0){
			if(daemon)
				syslog(LOG_INFO,"TFTP ERROR 4 Illegal file request, out of tftpboot folder");
			else
				fprintf(stderr,"TFTP ERROR 4 Illegal file request, out of tftpboot folder\n");
			err.code=htons(5);
			err.errorCode=4;
			err.zero='0';
			strcpy(err.message, "Not tftpboot folder\0");
			sendto(sockfd,&err , 5+  strlen(err.message), 0, &cli, clilen);
			return -1;
			}
		else
			result= fileName;
				
		
	} else{
		strcpy(temp,"/tftpboot/");
		result=strcat(temp,fileName);	
		}	
	 if(access(result ,F_OK) == -1 ){
		if(daemon)
			syslog(LOG_INFO,"TFTP ERROR 1 File not found");
		else 
			fprintf(stderr,"TFTP ERROR 1 File not found\n");
		err.code=htons(5);
		err.errorCode=1;
		err.zero='0';
		strcpy(err.message, "File not found");
		sendto(sockfd,&err , 5+  strlen(err.message), 0, &cli, clilen);
		return -1;
	}else if(access(result,R_OK)){
		if(daemon)
			syslog(LOG_INFO,"TFTP ERROR 2 Access violation");
		else
			fprintf(stderr,"TFTP ERROR 2 Access violation\n");
		err.code=htons(5);
		err.errorCode=2;
		strcpy(err.message, "Access error");
		sendto(sockfd,&err , 5+  strlen(err.message), 0, &cli, clilen);
		return -1;			
	}else{
		
		if(strcmp(mode,"netascii"))
			*file=fopen(result,"r");
		else if(strcmp(mode,"octet"))
			*file=fopen(result,"rb");
		else{
			if(daemon)
				syslog(LOG_INFO,"TFTP ERROR 4 Illegal option");
			else
				fprintf(stderr,"TFTP ERROR 4 Illegal option\n");
			err.code=htons(5);
			err.errorCode=4;
			err.zero='0';
			strcpy(err.message, "Invalid sending option");
			sendto(sockfd,&err , 5+  strlen(err.message), 0, &cli, clilen);
			return -1;
			}
		
			
		if(*file == NULL){
			if(daemon)
				syslog(LOG_INFO,"TFTP ERROR 0 Cant open file");
			else
				fprintf(stderr,"TFTP ERROR 0 Cant open file\n");
			err.code=htons(5);
			err.errorCode=0;
			err.zero='0';
			strcpy(err.message, "Can't open file");
			sendto(sockfd,&err , 5+  strlen(err.message), 0, &cli, clilen);
			return -1;
		}
	}
	return 0;
}


int daemon_init (const char *pname, int facility) {
	int i;
	pid_t pid;
	if ((pid = fork ()) < 0)
		return (-1);
	else if (pid)
		_exit (0); /* parent izlazi */
/* child 1 nastavlja ... */
	if (setsid () < 0) /* postaje session leader */
		return (-1);
	signal (SIGHUP, SIG_IGN); 
	if ((pid = fork ()) < 0)
		return (-1);
	else if (pid)
		_exit (0); /* child 1 zavrsava */
	/* child 2 nastavlja */
	chdir ("/"); /* change working directory */
	for (i = 0; i < 64; i++)
		close (i); /* zatvori sve file deskriptore */
	open("/dev/null", O_RDONLY); /* stdin > /dev/null */
	open("/dev/null", O_RDWR); /* stdout > /dev/null */
	open("/dev/null", O_RDWR); /* stderr > /dev/null */
	openlog(pname, LOG_PID, facility);
	return (0);
}

void Inet_ntop(int af, const void *src, char *dst, socklen_t size,int errnum){
	
	if(inet_ntop(af, src, dst, size) == NULL)
		errx(errnum , "Error in ntop conversion\n");
	
	}

void Getaddrinfo(char *adress,char *port,struct addrinfo *hints, 
	struct addrinfo **res, int errnum){
		
	int error;
	error = getaddrinfo(adress,port, hints, res);
	if(error)
		errx(errnum,"Error in getting adress");
	
}
	

int Socket(int family, int type, int protocol,int errnum){
	int sfd;
	if ((sfd= socket(family ,type, protocol))< 0) 
      errx(errnum, "Socket creation failure");
    return sfd;
	
}

void Listen(int sockfd, int queueSize, int errnum){
	
	if (listen(sockfd, queueSize) == -1) {
		errx(errnum,"listen failure");
    }
	
}


int Accept(int sockfd,struct sockaddr *addr, socklen_t *addrlen, int errnum ){
	int newfd;
	
	if ((newfd=accept(sockfd,addr,addrlen)) == -1) {
        errx(errnum,"Accept error");
    }
    return newfd;
	
}

void Send(int sockfd, void *msg, int len, int flags, int errnum){
	int bytesSentTotal = 0;
	int bytesSent;
	void *temp;
	int tempLen;
	bytesSent = send(sockfd,msg, len, flags);
    bytesSentTotal+=bytesSent;
    
    
    if (bytesSent == -1)
        errx(errnum, "Send failure");
        
	while(bytesSentTotal != len){
		printf("Error in sending large file, resending rest of chunk\n");
		temp = msg + bytesSent;
		tempLen = len - bytesSent;
		bytesSent = 0;
		bytesSent = send(sockfd,temp, len, flags);
		bytesSentTotal+=bytesSent;	
		}
	
	
}

int Recv(int sockfd, void *buf, int len, int flags, int errnum){
	int numbytes;
	if ((numbytes = recv(sockfd, buf, len, flags)) == -1) 
		errx(errnum,"Recieve failure");
    
	return numbytes;
}	



void Bind(int sfd, const struct sockaddr * addr, int addrlen, int errnum){
	
	if(bind(sfd,addr,addrlen) < 0 )
		errx(errnum, "Binding error");	
	
}

void Connect(int sockfd, struct sockaddr *serv_addr, int addrlen,int errnum){
	
	 if (connect(sockfd, serv_addr, addrlen) != 0) 
       errx(errnum,"Connection with the server failed...\n");
     else 
		printf("Connected to server\n");
	
}


int Poll(struct pollfd pfds[], nfds_t fd_count, int time, int errnum){
	
	int poll_count = poll(pfds, fd_count, time);

		if (poll_count == -1) {
			perror("poll\n");
			exit(errnum);
			}
	return poll_count;
		
	}


void add_to_pfds(struct pollfd *pfds[], int newfd, int *fd_count, int *fd_size)
 {
 if (*fd_count == *fd_size) {
 *fd_size *= 2; // Double it

 *pfds = realloc(*pfds, sizeof(**pfds) * (*fd_size));
 }

 (*pfds)[*fd_count].fd = newfd;
 (*pfds)[*fd_count].events = POLLIN; // Check ready-to-read

 (*fd_count)++;
 }
 
 
 
 
 void del_from_pfds(struct pollfd pfds[], int i, int *fd_count) {
 pfds[i] = pfds[*fd_count-1];
 (*fd_count)--;
}
