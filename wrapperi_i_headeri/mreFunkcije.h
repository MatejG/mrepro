

#define RECBUFFER_SIZE 1024*4

void Getaddrinfo(char *adress,char *port,struct addrinfo *hints, 
	struct addrinfo **res, int errnum);
int Socket(int family, int type, int protocol,int errnum);
void Bind(int sfd, const struct sockaddr * addr, int addrlen, int errnum);
int Accept(int sockfd,struct sockaddr *addr, socklen_t *addrlen, int errnum );
void Listen(int sockfd, int queueSize, int errnum);
void Send(int sockfd, const void *msg, int len, int flags, int errnum);
int Recv(int sockfd, void *buf, int len, int flags, int errnum);
void Connect(int sockfd, struct sockaddr *serv_addr, int addrlen,int errnum);
void Inet_ntop(int af, const void *src, char *dst, socklen_t size,int errnum);
void add_to_pfds(struct pollfd *pfds[], int newfd, int *fd_count, int *fd_size);
void del_from_pfds(struct pollfd pfds[], int i, int *fd_count);
int Poll(struct pollfd pfds[], nfds_t fd_count, int time, int errnum);
int daemon_init (const char *pname, int facility);
int checkFileTftp(int deamon,char * mode, char * fileName, int sockfd, struct sockaddr cli, int clilen ,FILE ** file);
int Recvfrom(int sfd,void *buff, size_t nbytes,int flags, 
struct sockaddr* cli, socklen_t *len);

struct ERROR{
	
	u_int16_t code;
	u_int16_t errorCode;
	char message[30];
	char zero;
	};
