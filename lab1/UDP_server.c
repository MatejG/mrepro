


#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "../wrapperi_i_headeri/mreFunkcije.h"

#define MAXLEN 6
#define S_PORT "1234"
#define MAX_PAYLOAD 512
#define DEF_PAYLOAD_SIZE 8


struct sigaction old_action;
char *payload;




//server shutdown
void sigint_handler(int sig_no)
{
	printf("Shuting down server/n");
    free(payload);
    sigaction(SIGINT, &old_action, NULL);
    kill(0, SIGINT);
}


int main (int argc, char *argv[])
{
	//variable declaration
	struct sigaction action;
	char buf[MAXLEN];
	char *port = S_PORT;
	struct sockaddr cli;
	struct addrinfo hints, *res;
	socklen_t clilen;
	int sfd,ch ,msglen;
	
	if(argc==1){   // no argument or option call (display info)
		fprintf( stderr,  "./UDP_server [-l port] [-p payload]\n");
		}
		
	//variable initialisation

	memset(&action, 0, sizeof(action));
    action.sa_handler = &sigint_handler;
    sigaction(SIGINT, &action, &old_action);
	payload = malloc (sizeof(char) * MAX_PAYLOAD);	
	strcat (payload,"PAYLOAD:");
		
	while((ch = getopt(argc, argv, "p:l:")) != -1){
		switch(ch){
			case 'l': 
					port = optarg;
					break;
			case 'p': 
					if(strlen(optarg)> (MAX_PAYLOAD-DEF_PAYLOAD_SIZE)){
						free(payload);
						errx(1,"payload too large");
					}
					strcat (payload, optarg);
					break;
			default: 
					return 2;
		}
	}
	
	printf("%s\n", payload);
	printf("port = %s\n", port);

	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 
	
	
	//setup server ports and bind 
	Getaddrinfo("127.0.0.1",port, &hints, &res,1);
	sfd = Socket(res -> ai_family , res->ai_socktype, res->ai_protocol, 2);
	Bind(sfd,res->ai_addr,res->ai_addrlen,3);

	
	//working loop
	while (1){
		printf("Waiting for bots\n");
		clilen = sizeof(cli);
		msglen = recvfrom(sfd, buf, MAXLEN, 0, &cli, &clilen);
		buf[msglen] = '\0' ;
		if(strcmp(buf,"HELLO") == 0){
			printf("Received request from bot\n");
			sendto(sfd, payload, strlen(payload), 0, &cli, clilen);
		}
		
	}
}
