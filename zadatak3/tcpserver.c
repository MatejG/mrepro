
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>

#include "../wrapperi_i_headeri/mreFunkcije.h"

#define S_PORT "1234"
#define BACKLOG 10


int checkFile(char * fileName, int newfd, FILE ** file){
	printf("Checking file\n");   
	if( strchr(fileName, '/')!= NULL){
		printf("Checking for / \n");
		Send(newfd,"3File must be from the same directory\0",38,0,11);
		close(newfd);
		return -1;
	}else if(access(fileName,F_OK) == -1 ){
		printf("Checking if exists \n");
		Send(newfd, "1File does not exist\0",21,0,11);
		close(newfd);
		return -1;
	}else if(access(fileName,R_OK)){
		printf("Checking if valid permission \n");
		Send(newfd,"2No permission to read the file\0",32,0,11);
		close(newfd);
		return -1;			
	}else{
		printf("Opening data stream\n");
		*file=fopen(fileName,"rb");
		if(*file == NULL){
			Send(newfd,"Failure to open file\0",22,0,11);
			close(newfd);
			return -1;
		}
	}
	printf("Stream opened \n");
	return 0;
}


struct recbuffer{
	int offset;
	char fileName[512];
	};


int 
main(int argc, char *argv[] )
{
    int sockfd, newfd;
    struct addrinfo hints, *res;
    struct sockaddr_in their_addr;
    socklen_t sin_size;
    int ch;
	char *port = S_PORT;
    int error;
    struct recbuffer fileNameBuffer;
    int one = 1;
    int offset;
    char buffer[RECBUFFER_SIZE];   //4KB file transfer buffer
    FILE * localfile;
    int bytesRead;
    
    
    
    	if(argc==1){   // no argument or option call (display info)
		printf("Usage: ./tcpserver [-p port]");	
		return 1;
		}

    
    while((ch = getopt(argc, argv, "p:")) != -1){
		switch(ch){
			case 'p': 
					port = optarg;
					printf("primio port %s\n",port);
					break;
		
			default: 
					return 4;
		}
	}
	
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype= SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

    Getaddrinfo("127.0.0.1", port, &hints, &res,5);
    sockfd = Socket(res -> ai_family, res -> ai_socktype, res -> ai_protocol, 6);
    
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	Bind(sockfd,res->ai_addr,res->ai_addrlen,7);
	
	freeaddrinfo(res);
    Listen(sockfd,BACKLOG, 8);

    //accept while loop
    
    while(1){
    
		sin_size = sizeof their_addr;
		printf("Waiting for connection\n");
		newfd=Accept(sockfd,(struct sockaddr *)&their_addr,&sin_size,9);

		Recv(newfd, &fileNameBuffer, sizeof(struct recbuffer), 0, 10);
	
		offset= ntohl(fileNameBuffer.offset);
		printf("Offset = %d\nDatoteka = %s\n", offset, fileNameBuffer.fileName);
    
		if(checkFile(fileNameBuffer.fileName,newfd,&localfile) != 0){
			printf("Bad request received, processing next client\n");
			continue;
			}
		memset(&buffer,0, RECBUFFER_SIZE);
		
		if(localfile == NULL){
			Send(newfd,"3Failure to open file\0",22,0,11);
			printf("Nesto grdo ne valja");
			close(newfd);
			return -1;
		}
		
		fseek(localfile, offset, SEEK_SET);  //set offset from start
	   
		printf("Sending first data packet\n");
		buffer[0]='0';
		if((bytesRead = fread(buffer + 1, 1, RECBUFFER_SIZE -1, localfile)) > 0){  //chunk for the first packet
			Send(newfd, buffer, bytesRead+1, 0, 11);
			memset(&buffer,0, RECBUFFER_SIZE);
			}
		while ((bytesRead = fread(buffer, 1, RECBUFFER_SIZE, localfile)) > 0){     //process data from file to socket in 4KB chunks
		
			Send(newfd, buffer, bytesRead, 0, 11);
			memset(&buffer,0, RECBUFFER_SIZE);                                                                      
			}
		
		printf("File sent succesfully\n");
		close(newfd);
		fclose(localfile);

		}//end accept while loop
	return 0;
}
