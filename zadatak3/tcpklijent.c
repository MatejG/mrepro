
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "../wrapperi_i_headeri/mreFunkcije.h"

#define MIN_ARG 1
 


struct requestbuffer{
	int offset;
	char fileName[512];
	};


int main(int argc, char **argv)
{
	
	char * fileName;
	int opt;
	int optionC= 0;
	char * serverAdress="127.0.0.1";
	char * serverPort = "1234";
	struct addrinfo hints, *res;
	int sockfd;
	char recBuffer[RECBUFFER_SIZE];
	int offset;
	struct requestbuffer sendRequestBuffer;
	FILE * localFile;
	int bytesReceived;
	int pom;
	
	
	if(argc==1){   // no argument or option call (display info)
		printf("Usage: ./tcpklijent [-s server] [-p port] [-c] filename\n");	
		exit(4);
		}

	
	while((opt=getopt(argc, argv,"s:p:c"))!=-1){
		switch(opt){                              
			
			case 's': 
				serverAdress = optarg;
				break;
			case 'p':
				serverPort = optarg;
				break;
			case 'c':
				optionC = 1;
				break;
			default:
				exit(5);
			}	
			
	}//end while
	
	
	if((argc - optind)!= MIN_ARG)    //if file not provided
		errx(6, "Usage: ./tcpklijent [-s server] [-p port] [-c] filename\n");
	
	fileName = argv[argc-1];
	printf("File name passed : %s\n", fileName);
	
	switch ( access( fileName, F_OK )){
		
		case -1:         //file ne postoji
			offset=0;
			printf("Making file\n");
			localFile = fopen(fileName, "wb");
				if (localFile == NULL) 
					errx(7, "Unable to open file\n");
			break;
		default:        //file postoji
			if(optionC == 0)
				errx(8, "File exists and option C not provided\n");
			else{                                  //file postoji i opcija C je zadana
				
				if(access (fileName, W_OK) == -1)
					errx(9, "No permission to write to file error\n");
				localFile = fopen(fileName, "a+b");
				if (localFile == NULL) 
					errx(10, "Unable to open file\n");
				fseek(localFile, 0L, SEEK_END);
				offset = ftell(localFile);	   //calculate offset
				printf("Calculated offset %d\n", offset);
				}
			
			}//end switch
				
				
		
		
	

	memset(&hints, 0, sizeof(struct addrinfo));
	memset(&sendRequestBuffer, 0, sizeof(struct requestbuffer));
	hints.ai_family = AF_INET;
	hints.ai_socktype= SOCK_STREAM;
	Getaddrinfo(serverAdress,serverPort,&hints,&res,11);
	
	sockfd = Socket(res -> ai_family, res -> ai_socktype, res -> ai_protocol, 12);
	Connect(sockfd, res -> ai_addr, res -> ai_addrlen, 13);
	
	freeaddrinfo(res);
	
	//send initial server request for file
	sendRequestBuffer.offset = htonl(offset);
	strcpy(sendRequestBuffer.fileName,fileName);
	
	Send(sockfd, &sendRequestBuffer,sizeof(sendRequestBuffer), 0, 14);  //send request to server;
	
	memset(&recBuffer,0,RECBUFFER_SIZE);
	bytesReceived = Recv(sockfd,recBuffer,RECBUFFER_SIZE,0,15);  //check first chunk 
	if(recBuffer[0] != '0'){
		close(sockfd);
		fclose(localFile);
		fprintf(stderr,"Server error message %c, received\n", recBuffer[0]);
		exit(recBuffer[0]);
		}
	else{
		pom = fwrite(recBuffer +1 , 1, bytesReceived -1, localFile);
		printf("Bytes received in first package: %d\n Bytes written %d\n", bytesReceived, pom);
		memset(&recBuffer,0,RECBUFFER_SIZE);
		}
	
	
	
	//receive rest of package
	 while((bytesReceived = Recv(sockfd,recBuffer,RECBUFFER_SIZE,0,15)) > 0 ){
		fwrite(recBuffer, 1, bytesReceived, localFile);
		printf("Bytes received : %d\n", bytesReceived);
		memset(&recBuffer,0,bytesReceived);
		}
	
		
	

	
	printf("File transfer complete\n");
	close(sockfd);
	fclose(localFile);

	
	return 0;
}

